.. this title is converted into a DOM id and used
   for populating this page using Github APIs,
   Do not edit it

.. _list:

List of projects
----------------

The following table is automatically extracted from our github organisation page
at [https://gitlab.com/ska-telescope]

================= ============= ===========
Gitlab repository Documentation Description
================= ============= ===========
testgit           testdoc       test description
testgit           testdoc       test description
================= ============= ===========

.. .. raw:: html

..   <script type="text/javascript" src="../_static/js/projects_list.js"></script>
